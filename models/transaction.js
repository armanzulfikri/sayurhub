const mongoose = require('mongoose');
const { Schema} =  mongoose;
// const uniqueValidator = require("mongoose-unique-validator");

const transactionSchema = new Schema(
  {
  cart : {
      type: Schema.Types.ObjectId, 
      ref: "Cart" 
  },
  first_name : {
    type : String,
    required : true,
  },
  last_name : {
    type : String,
    required : true,
  },
  email : {
    type : String,
    require : true,
    trim : false,
  },
  phone : {
    type : Number,
    require : true,
    trim : false,
  },
  address : {
    type : String,
    require : true,
  },
  status : { 
    type : String,
    required: true, //once payment done, the transaction turns out to success!,
    default: 'Success',
    enum: ['Success','On Process','Cancelled']
  },
  city : {
    type : String,
    required : true,
  },
  user: { type: Schema.Types.ObjectId, ref: "User", default: null },
}, { timestamps: true, versionKey: false })

const transaction = mongoose.model("Transaction", transactionSchema);

exports.Transaction = transaction;
