const mongoose = require("mongoose");
const { Schema } = mongoose;

const NotificationSchema = new mongoose.Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: "Admin", default: null },
    transaction: { type: Schema.Types.ObjectId, ref: "Transaction", default: null },
    
  },
  { timestamps: true }
);

const notification = mongoose.model("Notification", NotificationSchema);

exports.Notification = notification;
