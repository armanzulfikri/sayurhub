const mongoose = require('mongoose');
const { Schema } = mongoose;

const replySchema = new Schema(
    {
        reply: {
            type: String,
            default: null
        },

        discussion: { type: Schema.Types.ObjectId, ref: "Discussion", default: null },
        admin: { type: Schema.Types.ObjectId, ref: "Admin", default: null},
        user: { type: Schema.Types.ObjectId, ref: "User", default: null},
        

    },
    {
        timestamps: true
    }
)

const reply = mongoose.model("Reply", replySchema);

exports.Reply = reply;
