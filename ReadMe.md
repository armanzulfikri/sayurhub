# SayurHub
# Team SayurHub-Backend - E-Commerce Apps

Create a E-Commerce Apps (SayurHub)

Status Code Response
```
200 - OK                      > Call API success
201 - CREATED                 > Post success
202 - ACCEPTED                > Response/Post success
400 - BAD REQUEST             > Error on client side
404 - NOT FOUND               > Req.bodyrequest endpoint not found
409 - CONFLICT                > User not fill the requirement
500 - INTERNAL SERVER ERROR   > Error on server side
```
URL : https://pacific-oasis-23064.herokuapp.com

Image format must be in jpg, jpeg, png, svg.

# RESTful endpoints

## GET ((URL))/ : 
Homepage
```json
Request Header : not needed
```
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
    "message": "This is home page thanks."
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/user : 
Get All Users
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  success: true,
	message: "Successfully retrieve the data!",
	data: "<user data>"

}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/user/register : 
Register User
```json
Request Header : not needed
```
```json
Request Body: {
  "full_name": "<user name>",
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (201 - Created){
  {
    "success": true,
    "message": "Successfully create a user!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0,
        "createdAt": "<user time create>"
    }
  } 
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/user/login :
Login User
```json
Request Header : {
 not needed
}
```
```json
Request Body: {
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (200 - OK){
  success: true,
	message: "Successfully logged in!",
	token: "<your token>"
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find User or Password"
}
```

## PUT ((URL))/user/edit/:user_id : 
Edit Users
```json

Request Header : {
  "access_token": "<your access token">
}
```
```json

Request Body: {
  "full_name": "<user name>", 
  "profile_image" : "<user image>",
  "email": "<user email>", 
  "description": "<user description>"
}
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully update a user!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "<user description>",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0,
        "updatedAt": "<user time update>"
    }
  } 
}
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## DELETE ((URL))/user/delete/:user_jd: 
Delete Users
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## GetUserId ((URL))/user/id : 
GetUser By Id token
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
     "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "5f8ad3f14534ab053414b586",
        "full_name": "julia2",
        "email": "julia123@gmail.com",
        "password": "$2b$10$nsKb5YKYsRiFaPZdNGY6SeXG8USCapztMDsoB4Px260MAsUj9uule",
        "createdAt": "2020-10-17T11:22:25.426Z",
        "updatedAt": "2020-10-17T11:22:25.426Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## Get((URL))/user/notification : 
GetUser notification
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve notifications data, pretty",
    "data": {
        "user": "5fad01c63410723b700f3fed",
        "transaction": "5facfd79bdf0a93e30c51a4c",
        "_id": "5fad02303410723b700f3ff1",
        "__v": 0,
        "createdAt": "2020-11-12T09:36:48.587Z",
        "updatedAt": "2020-11-12T09:36:48.587Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## Get((URL))/user/notification/:notif_id : 
GetUser notification id
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve notifications data, pretty",
    "data": {
        "user": "5fad01c63410723b700f3fed",
        "transaction": "5facfd79bdf0a93e30c51a4c",
        "_id": "5fad02303410723b700f3ff1",
        "__v": 0,
        "createdAt": "2020-11-12T09:36:48.587Z",
        "updatedAt": "2020-11-12T09:36:48.587Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## post((URL))/user/notification/post/:transaction_id : 
Post transaction user
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Hey, you got a notification here. Check it out, you jackass!",
    "data": {
        "user": "5fad01c63410723b700f3fed",
        "transaction": "5facfd79bdf0a93e30c51a4c",
        "_id": "5fad07f08cad1d34e4fd467c",
        "__v": 0,
        "createdAt": "2020-11-12T10:01:20.098Z",
        "updatedAt": "2020-11-12T10:01:20.098Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
=======================================================================================================


## POST ((URL))/admin/login :
Login User
```json
Request Header : {
 not needed
}
```
```json
Request Body: {
  "email": "<user email>",
  "password": "<user password>"
}
```
```json
Response: (200 - OK){
  success: true,
	message: "Successfully logged in!",
	token: "<your token>"
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find admin"
}
```

## Get ((URL))admin/: 
Get admin data
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "data": {
        "_id": "5f9babf03a40f73378032c06",
        "full_name": "sayurhub",
        "email": "sayurhub@gmail.com",
        "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
        "createdAt": "2020-10-30T06:00:16.554Z",
        "updatedAt": "2020-10-30T06:00:16.554Z"
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
``` 

## Get ((URL))admin/data: 
Get all admin data
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "_id": "5f9babf03a40f73378032c06",
            "full_name": "sayurhub",
            "email": "sayurhub@gmail.com",
            "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
            "createdAt": "2020-10-30T06:00:16.554Z",
            "updatedAt": "2020-10-30T06:00:16.554Z"
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## Put ((URL))admin/edit/:id_admin: 
Edit data admin
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: {
  "full_name", "email"
}
```
```json

Response: (200 - OK){
    "success": true,
    "message": "Successfully edit data!",
    "data": [
        {
            "_id": "5f9babf03a40f73378032c06",
            "full_name": "sayurhub",
            "email": "sayurhub@gmail.com",
            "password": "$2b$10$347S0UUGrDomXOv5Oqcfnu3RtzJax8XVOKWiPczSi40MAwDFRJ.qS",
            "createdAt": "2020-10-30T06:00:16.554Z",
            "updatedAt": "2020-10-30T06:00:16.554Z"
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## Get ((URL))admin/user/: 
Get all Users 
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "profile_image": "https://res.cloudinary.com/waindinifitri/image/upload/v1602921267/ptb2eckxsuqh5qee689g.jpg",
            "description": "Jual Buah dan Sayur Murah",
            "transactions": "On progress",
            "_id": "5f89de133ef5c40f1cd3f9d9",
            "full_name": "julia",
            "email": "julia@gmail.com",
            "password": "$2b$10$uLLLq7.GrV/3wrXcIvMhcuAq24uhoXAq2U.5Z2RKK.ofdygiSqROC",
            "__v": 0,
            "updatedAt": "2020-10-17T07:54:27.998Z"
        },
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
``` 
## DELETE ((URL))admin/user/delete/:user_jd: 
Delete Users
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
        "description": "Please fill your description ",
        "transactions": "On progress",
        "_id": "<userId>",
        "full_name": "<user name>",
        "email": "<user email>",
        "password": "<user password>",
        "__v": 0
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST ((URL))admin/product/create : 
Create a product
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: {
    "product_name": "<product name>",
    "description": "<product description>",
    "category": "<product category>",
    "discount": "<product discount>",
    "price": "<product price>",
    "actualPrice": 0,
    "stock": "<product stock>",
    "weight": "<product weight>",
    "nutrition":"<product nutrition>",
    "farmer_supllier": "<product farmer_supllier>",
    "product_image": "<product image>",
}
```
```json

Response: (200 - OK){
   "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "price": "<product price>",
            "actualPrice": "<product actualPrice>",
            "stock": "<product stock>",
            "weight": "<product weight>",
            "nutrition":"<product nutrition>",
            "farmer_supllier": "<product farmer_supllier>",
            "product_image": "<product image>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT ((URL))admin/product/update : 
Update products
```json
Request Header : {
    "token": "<your token>" 
}
```
```json
Request Body: {
  Request Body: {
    "product_name": "<product name>",
    "description": "<product description>",
    "category": "<product category>",
    "discount": "<product discount>",
    "price": "<product price>",
    "actualPrice": 0,
    "stock": "<product stock>",
    "weight": "<product weight>",
    "product_image": "<product image>",
    "nutrition": "<product nutrition>",
    "farmer_supllier": "<product farmer_supllier>"
}
}
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Product  updated!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "price": "<product price>",
            "stock": "<product stock>",
            "weight": "<product weight>",
            "nutrition":"<product nutrition>",
            "farmer_supllier": "<product farmer_supllier>",
            "product_image": "<product image>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        }
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## DELETE ((URL))admin/product/delete/:products_id : 
Delete products
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully delete data!",
    "data": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "nutrition":"<product nutrition>",
        "farmer_supllier": "<product farmer_supllier>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))admin/transaction/all :

Get all the transaction by admin

```json
Request Header : {
  "token": "<admin token>"
}
```

```json
Request Body: not needed
```

```json
Response: (200 - OK){
 {
    "success": true,
    "message": "There is all the transaction data!",
    "data": [
        {
            "status": "Success",
            "user": null,
            "_id": "5fac0de810120335f0a319e0",
            "first_name": "waindini",
            "last_name": " nur fitri",
            "email": "dini1@hotmail.com",
            "phone": 8888888888888,
            "address": "Dimana mana saja hatiku senank",
            "totalPayment": 12000,
            "createdAt": "2020-11-11T16:14:32.140Z",
            "updatedAt": "2020-11-11T16:14:32.140Z"
        },
        {
            "status": "Success",
            "user": null,
            "_id": "5fac0e1e86221b25b45aadde",
            "first_name": "waindini",
            "last_name": " nur fitri",
            "email": "dini1@hotmail.com",
            "phone": 8888888888888,
            "address": "Dimana mana saja hatiku senank",
            "totalPayment": 12000,
            "createdAt": "2020-11-11T16:15:26.947Z",
            "updatedAt": "2020-11-11T16:15:26.947Z"
        },
        {
            "status": "Success",
            "user": "5f94a5e8e4a0c50c3ce1bf58",
            "_id": "5fac14352c8c8d3a3c1c6a9a",
            "first_name": "waindini",
            "last_name": "nur fitri",
            "email": "dini1@hotmail.com",
            "phone": 88888888888888,
            "address": "Dimana mana saja hatiku senank sekali",
            "totalPayment": 11000,
            "createdAt": "2020-11-11T16:41:25.479Z",
            "updatedAt": "2020-11-11T16:41:25.479Z"
        }
    ]
}
}
```

```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))admin/listnotification :

Get all notification by admin

```json
Request Header : {
  "token": "<admin token>"
}
```

```json
Request Body: not needed
```

```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve notification datas, jackass",
    "data": [
        {
            "user": "5f9bb89c9953b71ce8dd10ae",
            "transaction": null,
            "_id": "5facfe37823f612e80d9bf72",
            "__v": 0,
            "createdAt": "2020-11-12T09:19:51.569Z",
            "updatedAt": "2020-11-12T09:19:51.569Z"
        },
        {
            "user": "5f9bb89c9953b71ce8dd10ae",
            "transaction": "5facfd79bdf0a93e30c51a4c",
            "_id": "5facfe9a3410723b700f3fe9",
            "__v": 0,
            "createdAt": "2020-11-12T09:21:30.973Z",
            "updatedAt": "2020-11-12T09:21:30.973Z"
        },
        {
            "user": "5fad01c63410723b700f3fed",
            "transaction": "5facfd79bdf0a93e30c51a4c",
            "_id": "5fad02303410723b700f3ff1",
            "__v": 0,
            "createdAt": "2020-11-12T09:36:48.587Z",
            "updatedAt": "2020-11-12T09:36:48.587Z"
        },
        {
            "user": "5fad01c63410723b700f3fed",
            "transaction": "5facfd79bdf0a93e30c51a4c",
            "_id": "5fad07f08cad1d34e4fd467c",
            "__v": 0,
            "createdAt": "2020-11-12T10:01:20.098Z",
            "updatedAt": "2020-11-12T10:01:20.098Z"
        }
    ]
}
```

```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))admin/notification/:notification_id :

Get notification by id

```json
Request Header : {
  "token": "<admin token>"
}
```

```json
Request Body: not needed
```

```json
Response: (200 - OK){
    "success": true,
    "msg": "Successfully retrieve notification, jackass",
    "post": {
        "user": "5f9bb89c9953b71ce8dd10ae",
        "transaction": null,
        "_id": "5facfe37823f612e80d9bf72",
        "__v": 0,
        "createdAt": "2020-11-12T09:19:51.569Z",
        "updatedAt": "2020-11-12T09:19:51.569Z"
    }
}
```

```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
=======================================================================================================
## GET ((URL))/products : 
Get all products
```json

Request Query Params : {
  "page" : "<your page>"
}
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
     "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": [
        {
            "discount": "<product discount>",
            "_id": "<product id>",
            "product_name": "<product name>",
            "description": "<product description>",
            "category": "<product category>",
            "actualPrice": "<product actualPrice>",
            "price": "<product price>",
            "stock": "<product stock>",
            "product_image": "<product image>",
            "weight": "<product weight>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```


## GET ((URL))/products/:product_id:
Seacrh product by id
```json

Request Header : not needed
```
```json

Request Body: not needed
```
```json

Request Params: needed
```
```json

Response: (200 - OK){
   "success": true,
    "msg": "Succesfully retrieve all the products!",
    "products": {
        "discount": 0,
        "user": {
            "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
            "description": "Please fill your description ",
            "transactions": "On progress",
            "_id": "5f8ac46da59c4000172b9887",
            "full_name": "joko anwar",
            "email": "asd@asd.co",
            "password": "$2b$10$j9WmF47T/MKXMwsT8F6mDeY7wsJS94XxCChk8qOypJMJQGXjv7Rke",
            "createdAt": "2020-10-17T10:16:13.990Z",
            "updatedAt": "2020-10-17T10:16:13.990Z"
        },
        "_id": "5f956465eb16d90017356072",
        "__v": 0,
        "category": "Fruit",
        "createdAt": "2020-10-25T11:41:25.922Z",
        "description": "Buah yg ada di dengkul",
        "price": 5000,
        "product_image": "https://res.cloudinary.com/waindinifitri/image/upload/v1603626085/spmzghczzb60pgw84wkh.jpg",
        "product_name": "Coba buah beda token",
        "stock": 50,
        "updatedAt": "2020-10-25T11:41:25.922Z",
        "weight": 1
    }
}
```
```json

Response: (500 - Internal Server Error){
  "success" : false,
  "message" : "Cannot find User or Password"
}
```

## GET ((URL))/products/find/:product_name : 
Search products by product name


Request Header : not needed,
```
```json

Request params: {
  "product_name": "<product name>"
}
```
```json

Response: (200 - OK){
  {
    "success": true,
    "message": "Successfully retrieve all the products that have same name.",
    "data": {
       "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
  } 
}
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## GET ((URL))products/user : 
Delete products
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))products/filter/fruits : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))products/filter/diets : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))products/filter/vegetables : 
filter products
```json

Request Query Params : {
  "page" : "<your page>"
}
```
```json

Request Body: not needed
```
```json

Response: (200 - OK){
  {
    "success": true,
    "msg": "Successfully retrieve product data!",
    "userProducts": {
        "discount": "<product discount>",
        "_id": "<product id>",
        "product_name": "<product name>",
        "description": "<product description>",
        "category": "<product category>",
        "price": "<product price>",
        "actualPrice": "<product actualPrice>",
        "stock": "<product stock>",
        "weight": "<product weight>",
        "product_image": "<product image>",
        "createdAt": "<time createdAt>",
        "updatedAt": "<time updatedAt>",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
========================================================================================
## GET ((URL))/reviews : 
Get all reviews
```json
Request Header : not needed
```
```json

Request Body: not needed
```
```json

Response: (200 - OK) {
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "5f8d2c45f09e1305ac338af4",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e91f5f258e40c1064cbc3",
            "rating": 8,
            "review": "good",
            "createdAt": "2020-10-20T07:29:57.816Z",
            "updatedAt": "2020-10-20T07:31:15.377Z",
            "__v": 0
        },
        {
            "product": "5f8e7db207a0cb1a20b6e20d",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e947eb1b4361aac1c8c29",
            "rating": 10,
            "review": "very good",
            "createdAt": "2020-10-20T07:40:46.100Z",
            "updatedAt": "2020-10-20T07:40:46.100Z",
            "__v": 0
        },
        {
            "product": "5f8e7ea907a0cb1a20b6e20e",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e94b5b1b4361aac1c8c2a",
            "rating": 9,
            "review": "very good",
            "createdAt": "2020-10-20T07:41:41.737Z",
            "updatedAt": "2020-10-20T07:41:41.737Z",
            "__v": 0
        },
        {
            "product": "5f8d2c45f09e1305ac338af4",
            "user": "5f8e9c73d0650211f49e1dc2",
            "_id": "5f8e9d5b0048781c54cfbba6",
            "rating": 9,
            "review": "very good",
            "createdAt": "2020-10-20T08:18:35.227Z",
            "updatedAt": "2020-10-20T08:18:35.227Z",
            "__v": 0
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/reviews/create/:product_id
Create a review
```json
Request Header {
    "token" : "<your token>"
}
```
```json

Request Body: {
    "rating": "<rating>",
    "review": "<review>",
}
```
```json

Response: (200 - OK){
   "success": true,
    "message": "Successfully create a review!",
    "data": {
        "product": "5f8fd6bafe00881fecf66efe",
        "user": "5f8e9c73d0650211f49e1dc2",
        "_id": "5f8fdaaefe00881fecf66f01",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-21T06:52:30.291Z",
        "updatedAt": "2020-10-21T06:52:30.291Z",
        "__v": 0
    }
}
```
```json
Response: (409 - Conflict){
   "success": false,
    "message": "You have reviewed this product before!"
}
```

## PUT ((URL))/reviews/update/:review_id
Update review
```json
Request Header : {
    "token": "<your token>" 
}
Reques Param : needed
```
```json
Request Body: {
    "rating": "<rating>",
    "review": "<review>"
}
```
```json
Response: (201 - Created){
     "success": true,
    "message": "Successfully update a review!",
    "data": {
        "product": "5f8d2c45f09e1305ac338af4",
        "user": "5f8d8e5512114407f861462c",
        "_id": "5f8e91f5f258e40c1064cbc3",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-20T07:29:57.816Z",
        "updatedAt": "2020-10-21T07:04:05.414Z",
        "__v": 0
    }
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/reviews/product/:product_id
Get review by product_id
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
   "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "5f8e7db207a0cb1a20b6e20d",
            "user": "5f8d8e5512114407f861462c",
            "_id": "5f8e947eb1b4361aac1c8c29",
            "rating": 10,
            "review": "very good",
            "createdAt": "2020-10-20T07:40:46.100Z",
            "updatedAt": "2020-10-20T07:40:46.100Z",
            "__v": 0
        }
    ]
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
 
## DELETE ((URL))reviews/delete/:review_id
Delete review
```json

Request Header : {
  "token": "<your token">
}
```
```json

Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
    "success": true,
    "message": "Successfully deleted data!",
    "data": {
        "product": "5f8d2c45f09e1305ac338af4",
        "user": "5f8d8e5512114407f861462c",
        "_id": "5f8e91f5f258e40c1064cbc3",
        "rating": 9,
        "review": "very good",
        "createdAt": "2020-10-20T07:29:57.816Z",
        "updatedAt": "2020-10-21T07:04:05.414Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))/reviews/rating/:product_id
Get rating
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Request Param : needed
Response: (200 - OK){
    "average_rating": 9,
    "total_reviewer": 1,
    "Product": "5f8d2c45f09e1305ac338af4"
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
========================================================================================
## GET ((URL))/cart/view: 
Get list cart user
```json
Request Header : 
{token}

```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "status": true,
    "cart": [
        {
            "totalQty": 1,
            "totalPrice": 26000,
            "totalWeight": 1,
            "items": [
                {
                    "id": "5f9be34c774f1f1300e8d336",
                    "name": "Rock Melon",
                    "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
                    "price": 26000,
                    "weight": 1,
                    "stock": 10,
                    "subtotal": 26000,
                    "quantity": 1
                }
            ],
            "_id": "5fa3a5db01335a29089e20bc",
            "user": "5f9bb89c9953b71ce8dd10ae",
            "__v": 0
        }
    ]
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST ((URL))/cart/add/:product_id: 
Get list cart
```json
Request Header : not needed
{token}
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "totalQty": 1,
    "totalPrice": 26000,
    "totalWeight": 1,
    "items": [
        {
            "id": "5f9be34c774f1f1300e8d336",
            "name": "Rock Melon",
            "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
            "price": 26000,
            "weight": 1,
            "stock": 10,
            "subtotal": 26000,
            "quantity": 1
        }
    ],
    "_id": "5fa3a5db01335a29089e20bc",
    "user": "5f9bb89c9953b71ce8dd10ae",
    "__v": 0
}
```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## Delete ((URL))/cart/empty/:cart_id:
Empty all product in Cart
```json
Request Header : not needed
{token}
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Product deleted!",
    "doc": {
        "totalQty": 1,
        "totalPrice": 26000,
        "totalWeight": 1,
        "items": [
            {
                "id": "5f9be34c774f1f1300e8d336",
                "name": "Rock Melon",
                "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604051788/ey5otzzk6r8xc0vklq4h.jpg",
                "price": 26000,
                "weight": 1,
                "stock": 10,
                "subtotal": 26000,
                "quantity": 1
            }
        ],
        "_id": "5fa3a5db01335a29089e20bc",
        "user": "5f9bb89c9953b71ce8dd10ae",
        "__v": 0
    }
}

```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## Delete ((URL))/cart/delete/:cart_id/:product_id:
delete product in Cart
```json
Request Header : not needed
{token}
```json

Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Successfully retrieve product data",
    "product": {
        "totalQty": 1,
        "totalPrice": 9300,
        "totalWeight": 1,
        "items": [],
        "_id": "5fa80e9d67b6783614885b5a",
        "user": "5f9bb89c9953b71ce8dd10ae",
        "__v": 0
    }
}

```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

==================================================================================
## GET ((URL))/discussion/all :
Get all the discussions 
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully updated data!",
    "data": [
        {
            "product": "5f9bdf8a774f1f1300e8d330",
            "user": {
                "profile_image": "https://res.cloudinary.com/waindinifitri/image/upload/v1603680206/p7ic6ramw9vaffhnndiu.jpg",
                "description": "Yukika-chaan~",
                "transactions": "On progress",
                "_id": "5f9191e985144b001792849a",
                "full_name": "azani ramadhan",
                "email": "azani@gmail.com",
                "password": "$2b$10$pxNxHlLGeHugGU4Up2UQM.5g2uGDxqy3sqXQjpHhsyYRv9DZ4hT9S",
                "createdAt": "2020-10-22T14:06:33.614Z",
                "updatedAt": "2020-11-13T06:14:15.563Z"
            },
            "_id": "5fae23f6d610f50017380ee7",
            "createdAt": "2020-11-13T06:13:10.033Z",
            "updatedAt": "2020-11-13T06:46:58.526Z",
            "__v": 0
        },
        {
            "product": "5f9bef682cc604091026c770",
            "user": {
                "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
                "description": "Please fill your description ",
                "transactions": "On progress",
                "_id": "5f9bb89c9953b71ce8dd10ae",
                "email": "armanzulfikri@gmail.com",
                "password": "$2b$10$7lCe2EkRWuYqO.WYcbbF5.FwPb7EsPEI37eQlKFv3GEUamSdE5ce6",
                "createdAt": "2020-10-30T06:54:20.849Z",
                "updatedAt": "2020-11-13T07:10:49.094Z"
            },
            "_id": "5fae2d9c2f4dbd0ea4ed9f07",
            "write": "ready gk ?",
            "createdAt": "2020-11-13T06:54:20.707Z",
            "updatedAt": "2020-11-13T07:10:49.372Z",
            "__v": 0
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/discussion/:product_id :
Get all the discussions in one product
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully retrieve the data!",
    "data": [
        {
            "product": "5f9bef682cc604091026c770",
            "user": {
                "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
                "description": "Please fill your description ",
                "transactions": "On progress",
                "_id": "5f9bb89c9953b71ce8dd10ae",
                "email": "armanzulfikri@gmail.com",
                "password": "$2b$10$7lCe2EkRWuYqO.WYcbbF5.FwPb7EsPEI37eQlKFv3GEUamSdE5ce6",
                "createdAt": "2020-10-30T06:54:20.849Z",
                "updatedAt": "2020-11-13T07:10:49.094Z"
            },
            "_id": "5fae2d9c2f4dbd0ea4ed9f07",
            "write": "ready gk ?",
            "createdAt": "2020-11-13T06:54:20.707Z",
            "updatedAt": "2020-11-13T07:36:12.841Z",
            "__v": 0
        }
    ]
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## POST ((URL))/discussion/create/:product_id :
Create a discussion in a product by a user.
```json
Request Header : {
  "token": "<user token>"
}
```
```json

Request Body: {
  "write": "<write your discussion here/what do you want to ask about the product>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Discussion created!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}

```
```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## PUT ((URL))/discussion/edit/:discussion_id :
Edit the discussion in one product by the same user
```json
Request Header : {
  "token": "<user token>"
}
```
```json
Request Body: {
  "write": "<write your discussion here/what do you want to ask about the product>"
}
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully updated data!",
    "data": [
        {
            "product": "<product id>",
            "user": "<user id>",
            "_id": "<discussion id>",
            "write": "<user discussion/asking about the product>",
            "createdAt": "<time createdAt>",
            "updatedAt": "<time updatedAt>",
            "__v": 0
        },
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/reply/all :
Get all reply
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK){
    "success": true,
    "message": "Successfully get all data!",
    "data": [    {
            "reply": "masih ada sistah",
            "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
            "admin": "5f9babf03a40f73378032c06",
            "user": null,
            "_id": "5fae2e0e2f4dbd0ea4ed9f09",
            "createdAt": "2020-11-13T06:56:14.670Z",
            "updatedAt": "2020-11-13T06:56:14.670Z",
            "__v": 0
        },
        {
            "reply": "5fae2d9c2f4dbd0ea4ed9f07",
            "discussion": null,
            "admin": null,
            "user": {
                "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
                "description": "Please fill your description ",
                "transactions": "On progress",
                "_id": "5f9bb89c9953b71ce8dd10ae",
                "email": "armanzulfikri@gmail.com",
                "password": "$2b$10$7lCe2EkRWuYqO.WYcbbF5.FwPb7EsPEI37eQlKFv3GEUamSdE5ce6",
                "createdAt": "2020-10-30T06:54:20.849Z",
                "updatedAt": "2020-11-13T07:10:49.094Z"
            },
            "_id": "5fae2e4c38e0da09ec2b8123",
            "createdAt": "2020-11-13T06:57:16.496Z",
            "updatedAt": "2020-11-13T06:57:16.496Z",
            "__v": 0
        },
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## GET ((URL))/reply/:discussion_id :
Get reply by id discussion
```json
Request Header : not needed
```
```json
Request Body: not needed
```
```json
Response: (200 - OK) {
            "reply": "oke siap beli",
            "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
            "admin": null,
            "user": {
                "profile_image": "https://res.cloudinary.com/di02ey9t7/image/upload/v1602432289/FAVPNG_samsung-galaxy-a8-a8-user-login-telephone-avatar_peutPpGD_l18hzf.png",
                "description": "Please fill your description ",
                "transactions": "On progress",
                "_id": "5f9bb89c9953b71ce8dd10ae",
                "email": "armanzulfikri@gmail.com",
                "password": "$2b$10$7lCe2EkRWuYqO.WYcbbF5.FwPb7EsPEI37eQlKFv3GEUamSdE5ce6",
                "createdAt": "2020-10-30T06:54:20.849Z",
                "updatedAt": "2020-11-13T07:10:49.094Z"
            },
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST ((URL))/reply/add/:discussion_id :
admin reply discussion
```json
Request Header : 
<token Admin>
```
```json 
Request Body: 
<reply> :  your reply
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Reply created!",
    "replies": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": "5f9babf03a40f73378032c06",
        "user": null,
        "_id": "5fae2e0e2f4dbd0ea4ed9f09",
        "createdAt": "2020-11-13T06:56:14.670Z",
        "updatedAt": "2020-11-13T06:56:14.670Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## PUT ((URL))/reply/edit/:reply_id :
admin edit reply
```json
Request Header : 
<token Admin>
```
```json 
Request Body: 
<reply> :  your reply
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Successfully updated data!",
    "data": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": "5f9babf03a40f73378032c06",
        "user": null,
        "_id": "5fae2e0e2f4dbd0ea4ed9f09",
        "createdAt": "2020-11-13T06:56:14.670Z",
        "updatedAt": "2020-11-13T06:56:14.670Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## DELETE ((URL))/reply/delete/:reply_id :
admin delete reply
```json
Request Header : 
<token Admin>
```
```json 
Request Body: not needed
```json
Response: (200 - OK){
    "success": true,
    "msg": "Successfully deleted data!",
    "data": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": "5f9babf03a40f73378032c06",
        "user": null,
        "_id": "5fae2e0e2f4dbd0ea4ed9f09",
        "createdAt": "2020-11-13T06:56:14.670Z",
        "updatedAt": "2020-11-13T06:56:14.670Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST ((URL))/reply/user/create/:discussion_id :
user make reply in that discussion
```json
Request Header : 
<token User>
```
```json 
Request Body: 
<reply> :  your reply
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Reply created!",
    "replies": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": null,
        "user": "5f9babf03a40f73378032c06",
        "_id": "5fae376aa79a3d06acaeb762",
        "createdAt": "2020-11-13T07:36:10.797Z",
        "updatedAt": "2020-11-13T07:36:10.797Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## POST ((URL))/reply/user/edit/:reply_id :
user edit reply
```json
Request Header : 
<token User>
```
```json 
Request Body: 
<reply> :  your reply
```
```json
Response: (200 - OK)## POST ((URL))/reply/user/create/:discussion_id :
Get reply by id discussion
```json
Request Header : 
<token User>
```
```json 
Request Body: 
<reply> :  your reply
```
```json
Response: (200 - OK){
    "success": true,
    "msg": "Reply created!",
    "replies": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": null,
        "user": "5f9babf03a40f73378032c06",
        "_id": "5fae376aa79a3d06acaeb762",
        "createdAt": "2020-11-13T07:36:10.797Z",
        "updatedAt": "2020-11-13T07:36:10.797Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```
## DELETE ((URL))/reply/user/delete/:reply_id :
User delete reply
```json
Request Header : 
<token User>
```
```json 
Request Body: not needed
```json
Response: (200 - OK){
    "success": true,
    "msg": "Successfully deleted data!",
    "data": {
        "reply": "masih ada sistah",
        "discussion": "5fae2d9c2f4dbd0ea4ed9f07",
        "admin": "5f9babf03a40f73378032c06",
        "user": null,
        "_id": "5fae2e0e2f4dbd0ea4ed9f09",
        "createdAt": "2020-11-13T06:56:14.670Z",
        "updatedAt": "2020-11-13T06:56:14.670Z",
        "__v": 0
    }
}
```
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

==================================================================================
## POST ((URL))/transaction/checkout :

Create a checkout by a user.

```json
Request Header : {
  "token": "<user token>"
}
```

```json

Request Body: {
  "cart": "<write your cartId>",
  "first_name": "<write your first name>",
  "last_name": "<write your last name>",
  "email": "<write your emal account>",
  "phone": "<write your phone number>",
  "address": "<write your address detail>",
  "status": "<default status>",
  "city" : "<write your city>"
}
```

```json
Response: (201 - Created){
    "success": true,
    "msg": "Successfully created transaction!",
    "transaction": {
        "status": "Success",
        "user": "5fad01c63410723b700f3fed",
        "_id": "5fb1bb711aa0030530669d8c",
        "first_name": "Arman",
        "last_name": "Zulfikri",
        "email": "arman@gmail.com",
        "phone": 888888888888,
        "address": "Jln.nogonogo",
        "cart": {
            "totalQty": 3,
            "totalPrice": 14544,
            "totalWeight": 3,
            "items": [
                {
                    "id": "5f9bddcb774f1f1300e8d32e",
                    "name": "Mangga Harum Manis Imperfect (1kg)",
                    "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604050379/nine3qkyblzdvqi37kxl.jpg",
                    "price": 4968,
                    "weight": 1,
                    "stock": 10,
                    "subtotal": 4968,
                    "quantity": 1
                },
                {
                    "id": "5f9bddcb774f1f1300e8d32e",
                    "name": "Mangga Harum Manis Imperfect (1kg)",
                    "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604050379/nine3qkyblzdvqi37kxl.jpg",
                    "price": 4968,
                    "weight": 1,
                    "stock": 10,
                    "subtotal": 4968,
                    "quantity": 1
                },
                {
                    "id": "5f9c08840f835637c4992008",
                    "name": "Bawang Jadi Stroberi",
                    "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604206124/s2rz9wkiegfkheugehjq.jpg",
                    "price": 4608,
                    "weight": 1,
                    "stock": 5,
                    "subtotal": 4608,
                    "quantity": 1
                }
            ],
            "_id": "5faba99f9e4f3b4698aaccca",
            "user": "5f9bb89c9953b71ce8dd10ae",
            "__v": 2
        },
        "city": "yogyakarta",
        "createdAt": "2020-11-15T23:36:17.538Z",
        "updatedAt": "2020-11-15T23:36:17.538Z"
    }
}
```

```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/transaction/find :

Get the transaction by user

```json
Request Header : {
  "token": "<user token>"
}
```

```json
Request Body: not needed
```

```json
Response: (200 - OK){
    "data": [
        {
            "status": "Success",
            "user": "5fad01c63410723b700f3fed",
            "_id": "5fb1b05c8f235406d40291c8",
            "first_name": "Arman",
            "last_name": "Zulfikri",
            "email": "arman@gmail.com",
            "phone": 888888888888,
            "address": "Jln.nogonogo",
            "totalPayment": 9000000,
            "cart": {
                "totalQty": 3,
                "totalPrice": 14544,
                "totalWeight": 3,
                "items": [
                    {
                        "id": "5f9bddcb774f1f1300e8d32e",
                        "name": "Mangga Harum Manis Imperfect (1kg)",
                        "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604050379/nine3qkyblzdvqi37kxl.jpg",
                        "price": 4968,
                        "weight": 1,
                        "stock": 10,
                        "subtotal": 4968,
                        "quantity": 1
                    },
                    {
                        "id": "5f9bddcb774f1f1300e8d32e",
                        "name": "Mangga Harum Manis Imperfect (1kg)",
                        "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604050379/nine3qkyblzdvqi37kxl.jpg",
                        "price": 4968,
                        "weight": 1,
                        "stock": 10,
                        "subtotal": 4968,
                        "quantity": 1
                    },
                    {
                        "id": "5f9c08840f835637c4992008",
                        "name": "Bawang Jadi Stroberi",
                        "image": "https://res.cloudinary.com/waindinifitri/image/upload/v1604206124/s2rz9wkiegfkheugehjq.jpg",
                        "price": 4608,
                        "weight": 1,
                        "stock": 5,
                        "subtotal": 4608,
                        "quantity": 1
                    }
                ],
                "_id": "5faba99f9e4f3b4698aaccca",
                "user": "5f9bb89c9953b71ce8dd10ae",
                "__v": 2
            },
            "city": "yogyakarta",
            "createdAt": "2020-11-15T22:49:00.100Z",
            "updatedAt": "2020-11-15T22:49:00.100Z"
        }
    ]
}
```

```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```


==================================================================================

## POST ((URL))/delivery/cost :

Retrieve the cost delivery.

```json
Request Header : {
  "token": "<user token>"
}
```

```json

Request Body: {
  "origin": "<write your origin address shipping>",
  "destination": "<write your destination address>",
  "weight": "<your totalWeight of product>",
  "courier": "<choose your courier>"
}
```

```json
Response: (201 - Created){
    "success": true,
    "message": "Successfully retrieve the cost!",
    "data": {
        "rajaongkir": {
            "query": {
                "origin": "11",
                "destination": "27",
                "weight": 200,
                "courier": "pos"
            },
            "status": {
                "code": 200,
                "description": "OK"
            },
            "origin_details": {
                "city_id": "11",
                "province_id": "21",
                "province": "Nanggroe Aceh Darussalam (NAD)",
                "type": "Kabupaten",
                "city_name": "Aceh Utara",
                "postal_code": "24382"
            },
            "destination_details": {
                "city_id": "27",
                "province_id": "2",
                "province": "Bangka Belitung",
                "type": "Kabupaten",
                "city_name": "Bangka",
                "postal_code": "33212"
            },
            "results": [
                {
                    "code": "pos",
                    "name": "POS Indonesia (POS)",
                    "costs": [
                        {
                            "service": "Paket Kilat Khusus",
                            "description": "Paket Kilat Khusus",
                            "cost": [
                                {
                                    "value": 64000,
                                    "etd": "5-6 HARI",
                                    "note": ""
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }
}

```

```json

Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

## GET ((URL))/delivery/province :

Get the list of the province

```json
Request Header : {
  "token": "<user token>"
}
```

```json
Request Body: not needed
```

````json
Response: (201 - Created){
    "success": true,
    "message": "List of Province!",
    "data": {
        "rajaongkir": {
            "query": [],
            "status": {
                "code": 200,
                "description": "OK"
            },
            "results": [
                {
                    "province_id": "1",
                    "province": "Bali"
                },
                {
                    "province_id": "2",
                    "province": "Bangka Belitung"
                },
                {
                    "province_id": "3",
                    "province": "Banten"
                },
                {
                    "province_id": "4",
                    "province": "Bengkulu"
                },
                {
                    "province_id": "5",
                    "province": "DI Yogyakarta"
                },
                {
                    "province_id": "6",
                    "province": "DKI Jakarta"
                },
                {
                    "province_id": "7",
                    "province": "Gorontalo"
                },
                {
                    "province_id": "8",
                    "province": "Jambi"
                },
                {
                    "province_id": "9",
                    "province": "Jawa Barat"
                },
                {
                    "province_id": "10",
                    "province": "Jawa Tengah"
                },
                {
                    "province_id": "11",
                    "province": "Jawa Timur"
                },
                {
                    "province_id": "12",
                    "province": "Kalimantan Barat"
                },
                {
                    "province_id": "13",
                    "province": "Kalimantan Selatan"
                },
                {
                    "province_id": "14",
                    "province": "Kalimantan Tengah"
                },
                {
                    "province_id": "15",
                    "province": "Kalimantan Timur"
                },
                {
                    "province_id": "16",
                    "province": "Kalimantan Utara"
                },
                {
                    "province_id": "17",
                    "province": "Kepulauan Riau"
                },
                {
                    "province_id": "18",
                    "province": "Lampung"
                },
                {
                    "province_id": "19",
                    "province": "Maluku"
                },
                {
                    "province_id": "20",
                    "province": "Maluku Utara"
                }, (etc)
            ]
        }
    }
}
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
````

````
```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
````

## GET ((URL))/delivery/city :

Get the list of the city

```json
Request Header : {
  "token": "<user token>"
}
```

```json
Request Body: not needed
```

```json
Response: (201 - Created) {
    "success": true,
    "message": "List of City!",
    "data": {
        "rajaongkir": {
            "query": [],
            "status": {
                "code": 200,
                "description": "OK"
            },
            "results": [
                {
                    "city_id": "1",
                    "province_id": "21",
                    "province": "Nanggroe Aceh Darussalam (NAD)",
                    "type": "Kabupaten",
                    "city_name": "Aceh Barat",
                    "postal_code": "23681"
                },
                {
                    "city_id": "2",
                    "province_id": "21",
                    "province": "Nanggroe Aceh Darussalam (NAD)",
                    "type": "Kabupaten",
                    "city_name": "Aceh Barat Daya",
                    "postal_code": "23764"
                },
                {
                    "city_id": "3",
                    "province_id": "21",
                    "province": "Nanggroe Aceh Darussalam (NAD)",
                    "type": "Kabupaten",
                    "city_name": "Aceh Besar",
                    "postal_code": "23951"
                },
                {
                    "city_id": "4",
                    "province_id": "21",
                    "province": "Nanggroe Aceh Darussalam (NAD)",
                    "type": "Kabupaten",
                    "city_name": "Aceh Jaya",
                    "postal_code": "23654"
                },
                {
                    "city_id": "5",
                    "province_id": "21",
                    "province": "Nanggroe Aceh Darussalam (NAD)",
                    "type": "Kabupaten",
                    "city_name": "Aceh Selatan",
                    "postal_code": "23719"
                },{etc}
            ]
        }
    }
}
```

========================================================================================

## POST ((URL))/payment/charges :

Create a payment from a user.

```json
Request Header : {
  "token": "<user token>"
}
```

```json

Request Body: {
  "card_number": "<write your card_number>",
  "exp_month": "<write your exp_month>",
  "exp_year": "<write your exp_year>",
  "cvc": "<write your cvc>",
  "amount": "<write your amount>",
  "currency": "<write your currency>",
}
```

```json
Response: (201 - Created) {
    "success": true,
    "message": "Successfully paid!",
    "data": {
        "id": "ch_1HmN5sKfoWCNAY8xxkRudE9g",
        "object": "charge",
        "amount": 15000000,
        "amount_captured": 15000000,
        "amount_refunded": 0,
        "application": null,
        "application_fee": null,
        "application_fee_amount": null,
        "balance_transaction": "txn_1HmN5sKfoWCNAY8xlFLKfnjm",
        "billing_details": {
            "address": {
                "city": null,
                "country": null,
                "line1": null,
                "line2": null,
                "postal_code": null,
                "state": null
            },
            "email": null,
            "name": null,
            "phone": null
        },
        "calculated_statement_descriptor": "WAINDINI NUR FITRI",
        "captured": true,
        "created": 1605115612,
        "currency": "usd",
        "customer": null,
        "description": null,
        "destination": null,
        "dispute": null,
        "disputed": false,
        "failure_code": null,
        "failure_message": null,
        "fraud_details": {},
        "invoice": null,
        "livemode": false,
        "metadata": {},
        "on_behalf_of": null,
        "order": null,
        "outcome": {
            "network_status": "approved_by_network",
            "reason": null,
            "risk_level": "normal",
            "risk_score": 32,
            "seller_message": "Payment complete.",
            "type": "authorized"
        },
        "paid": true,
        "payment_intent": null,
        "payment_method": "card_1HmN5rKfoWCNAY8xVatU8sSU",
        "payment_method_details": {
            "card": {
                "brand": "visa",
                "checks": {
                    "address_line1_check": null,
                    "address_postal_code_check": null,
                    "cvc_check": "pass"
                },
                "country": "US",
                "exp_month": 12,
                "exp_year": 2022,
                "fingerprint": "BdFnWdCW6v2QeVaj",
                "funding": "credit",
                "installments": null,
                "last4": "4242",
                "network": "visa",
                "three_d_secure": null,
                "wallet": null
            },
            "type": "card"
        },
        "receipt_email": null,
        "receipt_number": null,
        "receipt_url": "https://pay.stripe.com/receipts/acct_1HehgHKfoWCNAY8x/ch_1HmN5sKfoWCNAY8xxkRudE9g/rcpt_IN7KF66TogWUrEk8xJ3uZzhsYltv9VA",
        "refunded": false,
        "refunds": {
            "object": "list",
            "data": [],
            "has_more": false,
            "total_count": 0,
            "url": "/v1/charges/ch_1HmN5sKfoWCNAY8xxkRudE9g/refunds"
        },
        "review": null,
        "shipping": null,
        "source": {
            "id": "card_1HmN5rKfoWCNAY8xVatU8sSU",
            "object": "card",
            "address_city": null,
            "address_country": null,
            "address_line1": null,
            "address_line1_check": null,
            "address_line2": null,
            "address_state": null,
            "address_zip": null,
            "address_zip_check": null,
            "brand": "Visa",
            "country": "US",
            "customer": null,
            "cvc_check": "pass",
            "dynamic_last4": null,
            "exp_month": 12,
            "exp_year": 2022,
            "fingerprint": "BdFnWdCW6v2QeVaj",
            "funding": "credit",
            "last4": "4242",
            "metadata": {},
            "name": null,
            "tokenization_method": null
        },
        "source_transfer": null,
        "statement_descriptor": null,
        "statement_descriptor_suffix": null,
        "status": "succeeded",
        "transfer_data": null,
        "transfer_group": null
    }
}
```

==================================================================================
## GET ((URL))/hystory/user :

Get transaction hystory user

```json
Request Header : {
  "token": "<user token>"
}
```

```json
Request Body: not needed
```

```json
Response: (200 - OK){
    "data": {
        "cart": null,
        "status": "On Process",
        "user": null,
        "_id": "5f94b65ae920be27a0e190d1",
        "count": 80000,
        "createdAt": "2020-10-24T23:18:50.745Z",
        "deliveries": "JNE",
        "product": null,
        "products": "Jeruk, Nanas, Kentang, Brokoli",
        "updatedAt": "2020-10-24T23:18:50.745Z"
    }
}
```

```json
Response: (500 - Internal Server Error){
  "<Error Message>"
}
```

