const { User } = require('../models/user');
const { Admin } = require('../models/admin');
const { Product } = require('../models/product');
const { Discussion } = require('../models/discussion');
const { Reply } = require('../models/reply');
// const mongoose = require('mongoose');

exports.Reply = async (req, res, next) => {
    try {
        let obj = {};
        const { reply } = req.body;
        const adminID = req.userData._id;
        const discussionID = req.params.discussion_id;

        //checking data input
        // if (write) obj.write = write;
        if (reply) obj.reply = reply;
        if (adminID) obj.admin = adminID;
        if (discussionID) obj.discussion = discussionID;

        // if (!(foundUser && foundProduct)) {
        let replies = await Reply.create(obj);
        await Admin.findByIdAndUpdate(adminID, {
            $push: { reply: reply._id },
        });
        await Discussion.findByIdAndUpdate(discussionID, {
            $push: { reply: reply._id },
        });

        res.status(201).json({
            success: true,
            msg: 'Reply created!',
            replies
        })
        console.log(replies)
        // } else {
        //     res.status(409).json({
        //         success: false,
        //         msg: 'Ada yang salah di controllernya!'
        //     })
        // }

    } catch (err) {
        next(err)
    }
}

exports.EditReply = async (req, res, next) => {
    try {
        const { id } = req.params;
        console.log(req.params)

        if (!id) return next({ message: "Missing ID Params" })

        const edit = await Reply.findByIdAndUpdate(
            id,
            { $set: req.body },
            { new: true }
        );
        res.status(200).json({
            success: true,
            message: "Successfully updated data!",
            data: edit,
        });
    }
    catch (err) {
        next(err);
    }
}

exports.GetReply = async (req, res, next) => {

    // if (productID) obj.product = productID;

    try {
        const id = req.params.discussion_id;

        const foundDiscussion = await Reply.find({
            discussion: id
        }).populate('user');
        if (foundDiscussion) {
            res.status(200).json({
                success: true,
                message: "Successfully retrieve the data!",
                data: foundDiscussion
            })

        } else {
            res.send(404).json({
                success: false,
                msg: "There is no discussion in this product yet!"
            })
        }
    } catch (err) {
        next(err);
    }
}

exports.DeleteReply = async (req, res, next) => {
    try {
        const id = req.params.id;
        
        await Reply.findByIdAndRemove(id, (err, doc, result) => {
            if (err) throw "Failed to delete";
            if (!doc) return res.status(400).json({
                success: false,
                err: "Data not found!"
            });
            res.status(200).json({
                success: true,
                message: "Successfully deleted data!",
                data: doc,
            });
        })
    }
    catch (err) {
        next(err);
    }
}

exports.allReply = async (req, res, next) => {
    try {
        const all = await Reply.find().populate('user')
            res.status(200).json({
                success: true,
                message: "Successfully get all data!",
                data: all,
            });
        
    }
    catch (err) {
        next(err);
    }
}
exports.ReplyUser = async (req, res, next) => {
    try {
        let obj = {};
        const { reply } = req.body;
        const userID = req.userData._id;
        const discussionID = req.params.discussion_id;

        //checking data input
        // if (write) obj.write = write;
        if (reply) obj.reply = reply;
        if (userID) obj.user = userID;
        if (discussionID) obj.discussion = discussionID;

        // if (!(foundUser && foundProduct)) {
        let replies = await Reply.create(obj);
        await User.findByIdAndUpdate(userID, {
            $push: { reply: reply._id },
        });
        await Discussion.findByIdAndUpdate(discussionID, {
            $push: { reply: reply._id },
        });

        res.status(201).json({
            success: true,
            msg: 'Reply created!',
            replies
        })
        console.log(replies)
        // } else {
        //     res.status(409).json({
        //         success: false,
        //         msg: 'Ada yang salah di controllernya!'
        //     })
        // }

    } catch (err) {
        next(err)
    }
}

exports.EditReplyUser = async (req, res, next) => {
    try {
        const id = req.params.reply_id;
        console.log(req.params)

        if (!id) return next({ message: "Missing ID Params" })

        const edit = await Reply.findByIdAndUpdate(
            id,
            { $set: req.body },
            { new: true }
        );
        res.status(200).json({
            success: true,
            message: "Successfully updated data!",
            data: edit,
        });
    }
    catch (err) {
        next(err);
    }
}

exports.GetReplyUser = async (req, res, next) => {
    try {
        const reply = req.params.reply_id;

        const foundDiscussion = await Reply.findOne({
            _id: reply
        }).populate('user');
        console.log(foundDiscussion);
        if (foundDiscussion) {
            res.status(200).json({
                success: true,
                message: "Successfully retrieve the data!",
                data: foundDiscussion
            })

        } else {
            res.send(404).json({
                success: false,
                msg: "There is no discussion in this product yet!"
            })
        }
        console.log(discussionID)
        console.log(foundDiscussion)
    } catch (err) {
        next(err);
    }
}

exports.DeleteReplyUser = async (req, res, next) => {
    try {
        const id = req.params.reply_id;

        await Reply.findByIdAndRemove(id, (err, doc, result) => {
            if (err) throw "Failed to delete";
            if (!doc) return res.status(400).json({
                success: false,
                err: "Data not found!"
            });
            res.status(200).json({
                success: true,
                message: "Successfully deleted data!",
                data: doc,
            });
        })
    }
    catch (err) {
        next(err);
    }
}
