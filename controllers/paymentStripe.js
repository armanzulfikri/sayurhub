const { Transaction } = require("../models/transaction");
const Stripe = require("stripe");
const stripe = Stripe(process.env.STRIPE_SECRET_KEY);
const axios = require('axios');
const qs = require("querystring");
//const mongoose = require('mongoose');

exports.stripeCharge = async (req, res, next) => {
	try {
	  let obj = {};
	  const {amount, currency, card_number, exp_month, exp_year, cvc} = req.body;
	  if(amount) obj.amount = Number(amount);
	  if(currency) obj.currency = currency;
	  if(card_number) obj.card_number = card_number;
	  if(exp_month) obj.exp_month = exp_month;
	  if(exp_year) obj.exp_year = exp_year;
	  if(cvc) obj.cvc = cvc;
		
	  
	  let token = await stripe.tokens.create({
		  card : {
			  number: card_number,
			  exp_month: exp_month,
			  exp_year: exp_year,
			  cvc: cvc,
		  },
	  });
	// let convert = Math.floor((amount/14.000)*100);
	// let chargeStripe = convert.toFixed(2)
	// console.log(chargeStripe)
	  const data = {
		  source: `${token.id}`,
		  amount: `${amount * 100}`,
		  currency: `${currency}`,
	  };
	  //console.log(process.env.STRIPE_SECRET_KEY);
	  const headers = {
		  Authorization: `Bearer ${process.env.STRIPE_SECRET_KEY}`,
		  "Content-Type": "application/x-www-form-urlencoded"
	  }
	  const datas = await axios.post("https://api.stripe.com/v1/charges", qs.stringify(data), {headers:headers})
	  //console.log(datas);
	  const invoice = datas.data.receipt_url;
	  res.status(201).json({
		success: true,
		message: "Successfully retrieve a receipt!",
		data : datas.data,
	}); 
	}
	catch (err) {
		next(err);
	}
};
