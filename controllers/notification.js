const { User } = require('../models/user');
const { Admin } = require('../models/admin');
const { Notification } = require('../models/notification');
const { Cart } = require('../models/cart');
const { Transaction } = require('../models/transaction');
// const { Pusher } = require('pusher');
const mongoose = require('mongoose');

exports.GetNotifications = async (req, res, next) => {
    try {
        const adminID = req.userData._id;
        const foundAdmin = await Admin.findOne({ admin: adminID._id });
        console.log(foundAdmin, "--admin");

        if (!foundAdmin) {
            next({ message: 'Missing adminID params' });
        }
        else {
            let post = await Notification.find()
            console.log(post, "--post admin")
            res.status(200).json({
                success: true,
                message: "Successfully retrieve notification datas",
                data: post
            })

        }

    } catch (err) {
        next(err)
    }
}

exports.GetYourNotifications = async (req, res, next) => {
    try {
        const userID = req.userData._id;
        
        let post = await Notification.findOne({user : userID});
            console.log(post, "--post user")
            res.status(200).json({
                success: true,
                message: "Successfully retrieve notifications data",
                data: post
            })
    } catch (err) {
        next(err)
    }
}

exports.adminGetNotificationsID = async (req, res, next) => {
    const adminID = req.userData._id;

    if (!adminID) return next({ message: 'Missing userID Params' });

    try {
        const { id } = req.params;
        let post = await Notification.findOne({ id });
        res.status(200).json({
            success: true,
            msg: 'Successfully retrieve notification',
            post
        })
    } catch (err) {
        next(err)
    }
}

exports.userGetNotificationsID = async (req, res, next) => {
    try {
        // const userID = req.userData._id;
        const  id  = req.params.notification_id;

        let post = await Notification.findOne({ _id:id });
        res.status(200).json({
            success: true,
            msg: 'Successfully retrieve notification, Good',
            data : post
        })
    } catch (err) {
        next(err)
    }
}

exports.postNotification = async (req, res, next) => {
    try {
        let obj = {};
        const userID = req.userData._id;
        const transaction = req.params.transaction_id;

        if (userID) obj.user = userID;
        if (transaction) obj.transaction = transaction;

        if (await Transaction.findOne({status: 'Success'})) {
            let adminNotification = await Notification.findByIdAndUpdate(
                { _id: mongoose.Types.ObjectId() },
                obj,
                {
                    new: true,
                    upsert: true,
                    runValidators: true,
                    setDefaultsOnInsert: true,
                    populate: { path: 'admin' }
                }
            )

            res.status(200).json({
                success: true,
                message: "Hey, you got a notification here. Check it out",
                data: adminNotification
            });
            console.log(adminNotification, '--adminNotification');
        } else {
            return 'Missing transaction status!'
        }

    } catch (err) {
        next()
    }
}
