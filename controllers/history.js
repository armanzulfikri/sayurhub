const { Transaction } = require('../models/transaction');

exports.getHistories = async(req, res, next) => {
    try {
        const userId = req.userData.userID
        const histories = await Transaction.findOne({user: userId});
        if (histories)
            res.status(200).json({
                data: histories,
            });
    } catch (err) {
        next(err);
    }
};
