const { Transaction } = require("../models/transaction");
const { Cart } = require("../models/cart");
const { Admin } = require('../models/admin');
const { User } = require('../models/user');
//const mongoose = require('mongoose');

exports.Create = async (req, res, next) => {
    try {
		let obj = {};
		// const cartId = req.params.cart_id;
		const userId = req.userData._id;
        const { cart,first_name, last_name, email, phone, address, status,city} = req.body;
		if(first_name) obj.first_name = first_name;
		if(last_name) obj.last_name = last_name;
		if(email) obj.email = email;
		if(phone) obj.phone =phone;
		if(address) obj.address = address;
		if(status) obj.status = 'Success!';
		if(cart) obj.cart = cart;
		if(userId) obj.user = userId;
		if(city) obj.city =  city;

		let transaction = await (await Transaction.create(obj)).execPopulate('cart');
		await User.findByIdAndUpdate(cart, {
			$push: {transaction: transaction._id},
		});
        res.status(201).json({
            success: true,
            msg: 'Successfully created transaction!',
            transaction
		})

    } catch (err) {
		console.log(err);
        next(err)
    }
}

exports.AllTransaction = async (req, res, next) => {
	try {
		const userId = req.userData._id
	  let transaction = await Transaction.find({user:userId}).populate('cart')
	  res.status(200).json({
		success: true,
		message: "There is all the transaction data!",
		data: transaction,
	  });
	} catch (err) {
	  next(err);
	}
  };
  
  exports.TransactionById = async (req, res, next) => {
	try {
	const  userId  = req.userData._id;
	  let transaction = await Transaction.find({user: userId}).populate('cart')
	  res.status(200).json({
		data: transaction,
	  });
	} catch (err) {
		console.log(err);
	  next(err);
	}
  };
  
//   exports.Delete = async (req, res, next) => {
// 	try {
// 	  const { id } = req.params;
  
// 	  if (!id) return next({ message: "Missing ID Params" });
  
// 	  await Transaction.findOneAndRemove(id, (error, doc, result) => {
// 		if (error) throw "Failed to delete";
// 		if (!doc)
// 		  return res.status(400).json({ success: false, err: "Transaction not found!" });
  
// 		res.status(200).json({
// 		  success: true,
// 		  message: "Successfully delete transaction data!",
// 		  data: doc,
// 		});
// 	  });
// 	} catch (err) {
// 	  next(err);
// 	}
//   };
