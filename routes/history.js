const express = require("express");
const router = express.Router();

const historyController = require("../controllers/history");
const { Authentication, IsAdmin } = require("../middlewares/auth");

// router.post("/add",Authentication, transactionController.Create);
// router.get("/all",IsAdmin, transactionController.AllTransaction);
router.get("/user", Authentication, historyController.getHistories);
// router.put("/update/:id",Authentication, transactionController.Edit);
// router.delete("/delete/:id",Authentication, transactionController.Delete);

module.exports = router;
