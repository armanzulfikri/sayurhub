const express = require("express");
const router = express.Router();

const replyController = require("../controllers/reply");
const { Authentication, IsAdmin } = require("../middlewares/auth");
// const {  } = require("../middlewares/auth")

router.get('/all', replyController.allReply)
router.get('/:discussion_id', replyController.GetReply);
router.post('/add/:discussion_id', IsAdmin, replyController.Reply);
router.put('/edit/:id', IsAdmin, replyController.EditReply);
router.delete('/delete/:id', IsAdmin, replyController.DeleteReply);

router.post('/user/create/:discussion_id', Authentication, replyController.ReplyUser);
router.put('/user/edit/:reply_id', Authentication, replyController.EditReplyUser);
router.delete('/user/delete/:reply_id', Authentication, replyController.DeleteReplyUser);
// router.get('/user/:reply_id', Authentication, replyController.GetReplyUser);/


module.exports = router;
