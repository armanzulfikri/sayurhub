const express = require("express");
const router = express.Router();

const adminControllers = require("../controllers/admin");
const userControllers = require("../controllers/user");
const productController = require("../controllers/product");
const transactionController = require("../controllers/transaction");
const notificationController = require('../controllers/notification');
const { IsAdmin } = require("../middlewares/auth");
const {uploader} = require("../middlewares/multer")
const { request } = require("express");

router.post("/login", adminControllers.Login);
router.get("/", IsAdmin, adminControllers.GetAdminId);
router.get("/data", IsAdmin, adminControllers.GetAdmin);



// routes User Admin
router.get("/user", IsAdmin, userControllers.GetUser);
router.delete("/user/delete/:id", IsAdmin, userControllers.Delete);



//routes product admin
router.post('/product/create', IsAdmin, uploader.single("product_image"), productController.Create)
router.put('/product/:id', IsAdmin, uploader.single("product_image"), productController.Update) 
router.delete('/product/delete/:id', IsAdmin, productController.Delete) 


//routes transaction
router.get("/transaction/all", IsAdmin, transactionController.AllTransaction);

//notification
router.get('/listnotification', IsAdmin, notificationController.GetNotifications)
router.get('/notification/:notification_id', IsAdmin, notificationController.adminGetNotificationsID)



module.exports = router;