const express = require("express");
const router = express.Router();

const discussionController = require("../controllers/discussion");
const { Authentication, IsAdmin } = require("../middlewares/auth");
// const {  } = require("../middlewares/auth")

router.get('/all', discussionController.allDiscussion);
router.get('/:product_id', discussionController.GetDiscussionByProduct);
router.post('/create/:product_id', Authentication, discussionController.CreateUser);
router.put('/edit/:id', Authentication, discussionController.EditDiscussion);
router.delete('/delete/:id', Authentication, discussionController.DeleteDiscussion);

module.exports = router;
